#include <iostream>
#include "complexe.hpp"


using namespace std;

//struct Complexe{
//	double rel, im;
//};
//
//
//void affiche(Complexe c){
//    char p = '+'?c.im>0 : ' ';
//    cout << c.rel << " " << p << c.im << "i" << endl;
//}
//
//Complexe add(Complexe c1, Complexe c2, bool printRes=true){
//	Complexe c;
//	c.im = c1.im + c2.im;
//	c.rel = c1.rel + c2.rel;
//	if(printRes)
//       affiche(c);
//
//	return c;
//}


int main()
{
  //    Complexe c1, c2, c3;
  //    c1.rel = 2;
  //    c1.im = 1;
  //
  //    c2.rel = 4;
  //    c2.im = -3;
  //
  //    affiche(c1);
  //    c3 = add(c1, c2, true);
  Complexe c = Complexe(2, -4);
  Complexe c2(3, 2);
  Complexe c3(1, 1);
  c.affiche();
  c.add(c2);
  c.affiche();
  c3.add(c).add(c2);
  c3.affiche();


  return 0;
}
