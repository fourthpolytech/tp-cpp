#ifndef COMPLEXE_H_INCLUDED
#define COMPLEXE_H_INCLUDED

#include <cstdlib>

class Complexe{
    private:
        double rel, im;

    public:
        Complexe(double r, double i);
        void affiche();
        Complexe add(Complexe c);
};



//Complexe add(Complexe c1, Complexe c2, bool printRes);
//
//void affiche(Complexe c);

#endif // COMPLEXE_H_INCLUDED
