#include <iostream>
#include "complexe.hpp"
using namespace std;

//Complexe add(Complexe c1, Complexe c2, bool printRes=true){
//	Complexe c;
//	c.im = c1.im + c2.im;
//	c.rel = c1.rel + c2.rel;
//	if(printRes)
//       affiche(c);
//
//	return c;
//}
//
//void affiche(Complexe c){
//    char p = '+'?c.im>0 : ' ';
//    cout << c.rel << " " << p << c.im << endl;
//}

Complexe::Complexe(double r, double i): rel(r), im(i){}

void Complexe::affiche(){
    char p = im>0 ? '+' : ' ';
    cout << rel << " " << p << " " << im <<"i" << endl;
    //printf("%lf %c %lfi\n", rel, p, im);
}

Complexe Complexe::add(Complexe c){
    im += c.im;
    rel += c.rel;

    return Complexe(im, rel);
}

