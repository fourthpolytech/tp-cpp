#include "shape.h"

Shape::Shape(){
  m_width = 0;
  m_height = 0;
}

Shape::Shape(double w, double h):m_width(w), m_height(h)
{ }

Shape::Shape (const Shape &s) : m_width(s.m_width), m_height(s.m_height)
{ }

Shape & Shape::operator=(const Shape &s){
  m_width = s.m_width;
  m_height = s.m_height;
  return *this;
}
  
Shape::~Shape(){}

