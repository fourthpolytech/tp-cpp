#include "shape.h"
#include "cmath"

class Rectangle : public Shape{

  
 public :
  Rectangle ();
  Rectangle (double w, double h);
  Rectangle & operator=(const Rectangle &tr);
  virtual double area()const;
  virtual double perimeter()const;
  virtual void affiche(std::ostream &op)const;
};
