#include <iostream>

#include "shape.h"
#include "triangleRect.h"
#include "rectangle.h"

std::ostream &operator << (std::ostream &os, Shape const &s){
  s.affiche(os);
  return os;
}


int main(){
  
  Rectangle tr (3.0, 4.0);
  Shape *rect = new Rectangle(2.0, 3.0);

  // std::cout << "le perimetre = " << tr.perimeter() << "\nla surface = "<< tr.area() << "\n";

  std::cout << *rect << "\n";

  return 0;
}


