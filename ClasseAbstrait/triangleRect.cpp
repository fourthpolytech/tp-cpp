#include "triangleRect.h"

TriangleRectangle::TriangleRectangle () : Shape() {}

TriangleRectangle::TriangleRectangle (double w, double h) : Shape(w, h){}

TriangleRectangle & TriangleRectangle::operator=(const TriangleRectangle &tr){
  this->Shape::operator=(tr);

  return *this;
}

double TriangleRectangle::perimeter() const{
  return (m_width + m_height)*2;
}

double TriangleRectangle::area() const{
  return m_width * m_height;
}

void TriangleRectangle::affiche(std::ostream &op) const{
  op << "Triange rectangle de perimetre " << perimeter() << " et de surface " << area() ;
}
