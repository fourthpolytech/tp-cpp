#include "rectangle.h"

Rectangle::Rectangle () : Shape() {}

Rectangle::Rectangle (double w, double h) : Shape(w, h){}

Rectangle & Rectangle::operator=(const Rectangle &tr){
  this->Shape::operator=(tr);

  return *this;
}

double Rectangle::perimeter()const{
  return (m_width + m_height)*2;
}

double Rectangle::area()const{
  return m_width * m_height;
}

void Rectangle::affiche(std::ostream &op)const{
  op << "Triange rectangle de perimetre " << this->perimeter() << " et de surface " << this->area() ;
}
