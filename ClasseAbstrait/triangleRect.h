#include "shape.h"
#include "cmath"

class TriangleRectangle : public Shape{

  
 public :
  TriangleRectangle ();
  TriangleRectangle (double w, double h);
  TriangleRectangle & operator=(const TriangleRectangle &tr);
  virtual double area()const;
  virtual double perimeter()const;
  virtual void affiche(std::ostream &op)const;
};
