#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstdio>

class Shape {
 protected:
  double m_width;
  double m_height;

 public:
  Shape(double w, double h);
  Shape();
  Shape (const Shape &s);
  Shape & operator=(const Shape &s);
  virtual ~Shape();
  virtual double area()const=0;
  virtual double perimeter()const=0;
  virtual void affiche(std::ostream &op)const=0;

  friend std::ostream &operator << (std::ostream &os, Shape const &s);
  
  
  
};

#endif //SHAPE_H
