#ifndef MATERIEL_H
#define MATERIEL_H

class Materiel
{
 protected:
  int m_id;

 public :
  Materiel();
  Materiel(int id);
  int getId() const;
};

#endif
