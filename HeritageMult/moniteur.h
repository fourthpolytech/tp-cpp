#ifndef MONITEUR_H
#define MONITEUR_H

#include <iostream>

#include "materiel.h"

class Moniteur: public virtual Materiel
{
 protected:
  //int m_id;
  int m_resolutionX;
  int m_resolutionY;

 public:
  Moniteur();
  Moniteur(int id, int resolX, int resolY);
  Moniteur(int resolX, int resolY);
  //int getId() const;
};

#endif
