#include "ordinateur.h"

Ordinateur::Ordinateur(): Materiel(), CarteMere(), Moniteur(), m_nbDisqueDur(0), m_graveurCd(false)
{}

Ordinateur::Ordinateur(int id, std::string processeur, int resolX, int resolY, int nbDisque, bool graveur):
  Materiel(id), CarteMere(processeur), Moniteur(resolX, resolY), m_nbDisqueDur(nbDisque), m_graveurCd(graveur)
{}
