#ifndef CARTEMERE_H
#define CARTEMERE_H

#include <iostream>

#include "materiel.h"

class CarteMere : public virtual Materiel
{
 protected:
  //int m_id;
  std::string m_typeProcess;

 public:
  CarteMere();
  CarteMere(int id, std::string processeur);
  CarteMere(std::string processeur);
  //int getId() const;
};

#endif
