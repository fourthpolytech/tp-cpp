#include "moniteur.h"

Moniteur::Moniteur(): Materiel(), m_resolutionX(0), m_resolutionY(0)
{}

Moniteur::Moniteur(int id, int resolX, int resolY):
  Materiel(id), m_resolutionX(resolX), m_resolutionY(resolY)
{}


Moniteur::Moniteur(int resolX, int resolY) : m_resolutionX(resolX), m_resolutionY(resolY)
{}

// int Moniteur::getId() const
// {
//   return m_id;
// }
