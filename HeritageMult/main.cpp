#include <iostream>

#include "carteMere.h"
#include "moniteur.h"
#include "ordinateur.h"

int main(){
  CarteMere c;
  Moniteur m;
  Ordinateur ordi (23, "intel", 1240, 800, 2, true);
  std::cout << "id pour carte mere = " << ordi.getId() << "\n";
  std::cout << "id pour moniteur = " << ordi.getId() << "\n";
  return 0;
}
