#ifndef ORDINATEUR_H
#define ORDINATEUR_H

#include <ostream>

#include "carteMere.h"
#include "moniteur.h"

class Ordinateur: public CarteMere, public Moniteur
{
 private:
  int m_nbDisqueDur;
  bool m_graveurCd;

 public :
  Ordinateur();
  Ordinateur(int id, std::string processeur, int resolX, int resolY, int nbDisque, bool graveur);
  

  
};

#endif
