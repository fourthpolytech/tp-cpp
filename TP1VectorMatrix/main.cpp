#include <iostream>
#include "vector.h"
#include "matrix.h"


using namespace std;

void affiche(const Matrix &M){
  for(int i=0; i<M.m_m; i++){
    for(int j=0; j< M.m_n; j++){
      std::cout << M.m_M[i][j] << " ";
    }
    std::cout << "\n";
  }
}

int main()
{
  vector v(5);
  vector v2(2);
  v.setElement(0, 2);
  v.setElement(1, 5);
  v.setElement(2, 3);
  v.setElement(3, 6);
  v.setElement(4, 8);

  v2.setElement(0, 10);
  v2.setElement(1, 16);

  vector v3 = v;
  vector v4(v2);
  
  v.printElement();

  int *t = v.getV();
  t[0] = 3;

  v.printElement();

  std::cout << "le nombre d'instance est : " << vector::getCpt() << "\n";

  std::cout << "le nombre d'instance est : " << vector::getCpt() << "\n";
  v2.printElement();
  v3.printElement();
  v4.printElement();
  

  std::cout << v.isInLimits(3, 1, 2) << "\n";

  Matrix m;
  m.init();
  affiche(m);
  

  return 0;
}


