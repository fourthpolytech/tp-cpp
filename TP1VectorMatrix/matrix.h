#ifndef MATRIX_H
#define MATRIX_H
#include <cstdlib>
#include <cstdio>
#include <iostream>

class Matrix{
 private :
  const int  m_m = 3;
  const int  m_n = 2;
  int **m_M;

 public :
  Matrix();
  //Matrix(int m, int n);
  void init();
  int getElement(int line, int col);
  void setElement(int line, int col, int val);
  //friend void mulMatVec(const Matrix &M, const Vector &v, Vector &res);
  friend void affiche(const Matrix &M);
};


#endif
