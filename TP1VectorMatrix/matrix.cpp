#include "matrix.h"

Matrix::Matrix(){
  // m_m = 0;
  // m_n = 0;
  m_M = new int*[m_m];
  for(int i=0; i<m_m; i++){
    m_M[i] = new int[m_n];
  }
}

// Matrix::Matrix(int m, int n){
//   m_m = m; m_n = n;
//   m_M = new int[m_m][m_n]; 
// }

void Matrix::init(){
  srand (time(0));
  for(int i=0; i<m_m; i++){
    for(int j=0; j<m_n; j++){
      m_M[i][j] = rand()%10 + 1;
    }
  }
}

int Matrix::getElement(int line, int col){
  return m_M[line][col];
}

void Matrix::setElement(int line, int col, int val){
  m_M[line][col] = val;
}
