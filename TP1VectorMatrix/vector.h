#ifndef VECTOR_H
#define VECTOR_H
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "matrix.h"

class vector
{
 public:
  vector(int size);
  vector();
  vector(const vector&  v);
  vector& operator=(const vector &v);
  virtual ~vector();
  int * getV() const;
  int getElement(int index);
  void setElement(int index, int value);
  //void affiche(const Matrix &M);
  void printElement();
  static int getCpt(){
    return m_cpt;
  }
  inline bool isInLimits(int idx, int lim1, int lim2){
    return (idx >=lim1 && idx<=lim2) ? true : false;
  }

 private:
  static int m_cpt;
  int m_size;
  int * m_v;
};

#endif // VECTOR_H
