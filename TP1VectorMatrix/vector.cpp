#include "vector.h"
#include "matrix.h"

int vector::m_cpt = 0;

vector::vector()
{
  m_size = 0;
  m_v = new int[m_size];
  m_cpt += m_size;
}

vector::vector(const vector&  v)
{
  m_size = v.m_size;
  m_v = v.getV();
}

vector& vector::operator= (const vector &v)
{
  m_size = v.m_size;
  m_v = v.getV();
  return *this;
}

vector::~vector()
{
  delete [] m_v;
}

//constructeur pour allouer le tableau
vector::vector(int size) {
  this->m_size = size;
  m_v = new int[m_size] ;
  m_cpt += m_size;
}

//accesseur du tableau d'elements
int * vector::getV() const{
  int *copy = new int[this->m_size];
  for(int i=0; i<m_size; i++){
    copy[i] = m_v[i];
  }
  return copy;
  //return m_v;
}

int vector::getElement(int index){
  return (index < m_size) ? m_v[index] : 0;
}

void vector::setElement(int index, int value){
  if(index < m_size)
    m_v[index] = value;
}

void vector::printElement(){
  for(int i=0; i<m_size; ++i){
    std::cout << m_v[i] << " ";
  }
  std::cout << "\n";
}

//méthode amie
// void affiche(const Matrix &M){
//   for(int i=0; i<M.m_m; i++){
//     for(int j=0; j< M.m_n; j++){
//       std::cout << M.m_M[i][j] << " ";
//     }
//     std::cout << "\n";
//   }
// }

// static int vector::getCpt(){
//   return m_cpt;
// }

// inline bool vector::isInLimits(int idx, int lim1, int lim2){
//   //return (idx >=lim1 && idx<=lim2) ? true : false;
//   if(idx >=lim1 && idx<=lim2)
//     return true;
//   return false;
// }
