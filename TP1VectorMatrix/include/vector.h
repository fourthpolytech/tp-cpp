#ifndef VECTOR_H
#define VECTOR_H


class vector
{
    public:
        vector(int size);
        vector();
        virtual ~vector();
        int *getV();

    protected:

    private:
        int m_size;
        int * m_v;
};

#endif // VECTOR_H
