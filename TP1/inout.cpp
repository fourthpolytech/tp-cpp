#include <iostream>
#include <cstdlib>

int main(int argc, char* argv[])
{
	int param, num;

	// argc contient le nombre d'arguments (y compris le nom de l'exécutable)
	if (argc != 2) {
		std::cout << "Please give exactly one argument." << std::endl;
        	std::exit(-1);
	}

	// argv[0] est le nom de l'exécutable, argv[1] est le premier argument, etc.
	param = std::atoi(argv[1]);

	std::cout << "Give one more number: ";
	std::cin >> num;

	std::cout << "The numbers are: " << param << ", " << num << std::endl;
	
	return 0;
}
