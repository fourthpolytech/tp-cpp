#include <iostream>

#include "personne.h"
#include "date.h"
#include "etudiant.h"
//#include "employe.h"
#include "manager.h"

int main(){
  Personne p ("bidule", "truc", Date(5, 12, 2010));
  Personne pc(p); // copie
  Personne pa ; //affectation
  pa = p;
  std::cout << "#####TEST DE PERSONNE #####\n";
  // std::cout << "tu t'appelles : " << p.getNom() << " " << p.getPrenom() << " tu as "<< p.getAge() << " ans(s)"<< "\n";
  p.print();
  std::cout << "APRES COPIE\n";
  pc.print();
  std::cout << "APRES AFFECTATION\n";
  pa.print();
  
  std::cout << "\n###### TEST DE ETUDIANT #####\n";
  Etudiant e ("toto", "tata", Date(23, 9, 2009), "etud1", "univ");
  e.print();
  Etudiant ec(e);
  std::cout << "APRES COPIE\n";
  ec.print();
  Etudiant ea;
  ea = e;
  std::cout << "APRES AFFECTATION\n";
  ea.print();

  std::cout << "\n###### TEST DE EMPLOYE #####\n";
  Employe emp1 ("papi", "mbaye", Date(6, 1, 1998), 3000, "polytech");
  Employe emp2 ("seydi", "peulh", Date(3, 4, 1993), 3000, "polytech");
  emp1.print();
  emp2.print();
  std::cout << "APRES COPIE\n";
  Employe emp3 (emp1);
  emp3.print();
  std::cout << "APRES AFFECTATION\n";
  emp2 = emp1;
  emp2.print();

  std::cout << "\n###### TEST DE MANAGER #####\n";
  Manager m1 ("papi", "mbaye", Date(6, 1, 1998), 3000, "polytech", 2, 1000);
  m1.addToTeam(emp1);
  m1.addToTeam(emp2); //SEG FAULT
  std::cout << m1.addToTeam(emp3) << "\n";
  m1.print();

  std::cout << "\n###### TEST DE L'HERITAGE #####\n";
  Personne *p1 = &e ;
  Personne *p2 = &emp1;
  Personne *p3 = &m1;
  p1->print();
  p2->print();
  p3->print();
  Personne* pp = new Manager("Poldo", "Sbaffini", Date(1929, 1, 17), 1500, "KF", 10, 200);

  delete pp;
}
