#ifndef DATE_H
#define DATE_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#

class Date{
 private :
  int m_day;
  int m_month;
  int m_year;
  static Date m_today;
  //enum month: int{Janvier=1, Fevrier, Mars, Avril, Mai, Juin, Juillet, Aout, Septembre, Octobre, Novembre, Decembre};

 public:
  Date(int d, int m, int y);
  Date();
  Date (const Date &v);
  Date& operator= (const Date &v);
  ~Date();
  void afficheDate();
  int getAge(const Date &d) const;
  static const Date getTodayDate();
  
};


#endif 
