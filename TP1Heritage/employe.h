#ifndef EMPLOYE_H
#define ETUDIANT_H

#include <iostream>
#include <string>

#include "personne.h"

class Employe: public Personne{
 protected :
  float m_salaire;
  std::string m_nomEntreprise;

 public:
  Employe();
  Employe(const std::string &nom, const std::string &prenom, const Date &naiss, const float salaire, const std::string &nomEnt);
  Employe(const Employe &emp);
  virtual ~Employe();
  Employe & operator=(const Employe &emp);
  std::string getNomEntre();
  float getSalaire();
  void setNomEntre(const std::string &nom);
  void setSalaire(const float sal);
  virtual void print();
};

#endif
