#include "etudiant.h"
// constructeur par défaut
Etudiant::Etudiant ()
{
  m_numCarteEtud = "" ;
  m_nomUniv = "";
}

// Constructeur avec paramètres
Etudiant::Etudiant(const std::string &nom, const std::string &prenom, const Date &naiss, 
	 const std::string &num, const std::string &univ) :
  Personne(nom, prenom, naiss), m_numCarteEtud(num), m_nomUniv(univ)
{}

  // Constructeur par copy
Etudiant::Etudiant(const Etudiant &etud) :
  Personne(etud), m_numCarteEtud(etud.m_numCarteEtud), m_nomUniv(etud.m_nomUniv)
{
}

//con

// Affectation
Etudiant & Etudiant::operator=(const Etudiant &etud) 
{
  //A COMPLETER Pour personne
  this->Personne::operator=(etud); //operateur de la classe mere
  m_numCarteEtud = etud.m_numCarteEtud;
  m_nomUniv = etud.m_nomUniv;
  return *this;
}

// accesseur de num
std::string Etudiant::getNum() const
{
  return std::string(m_numCarteEtud);
}

// accesseur de nomUniv
std::string Etudiant::getUniv() const
{
  return std::string(m_nomUniv);
}

// mutateur de Num
void Etudiant::setNum(const std::string &num)
{
  m_numCarteEtud = num;
}

// mutateur de nomUniv
void Etudiant::setUniv(const std::string &univ)
{
  m_nomUniv = univ;
}

//REDEFINIR PRINT
void Etudiant::print(){
  Personne::print();
  std::cout << "n° " << m_numCarteEtud << "universté : " << m_nomUniv << "\n";
}
