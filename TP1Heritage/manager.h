#ifndef MANAGER_H
#define MANAGER_H

#include "employe.h"

class Manager: public Employe
{
 protected :
  int m_capacite; //capacite max d'employe dans l'equipe
  Employe *m_equipe;
  int m_taille;	  /* le nombre actuel d'employe */
  float m_primeAnnuelle;

 public:
  Manager();
  Manager(const std::string &nom, const std::string &prenom, const Date &naiss,
	  const float salaire, const std::string &nomEnt, const int &capacite, const float &prime);
  Manager(const Manager &m);
  Manager & operator= (const Manager &m);
  ~Manager();
  const Employe* getEquipe();
  void setPrimeAnnuelle(float prime);
  virtual void print();
  bool addToTeam(const Employe &e);
  
  
};

#endif
 
