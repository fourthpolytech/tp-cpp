#include "date.h"

Date Date::m_today;

Date::Date(int d, int m, int y) : m_day(d), m_month(m), m_year(y) {}

Date::Date(){}

Date::~Date(){}

Date::Date(const Date &v) : m_day(v.m_day), m_month(v.m_month), m_year(v.m_year)
{}

Date& Date::operator= (const Date &v){
  m_day = v.m_day;
  m_month = v.m_month;
  m_year = v.m_year;

  return *this;
}

void Date::afficheDate(){
  //month m = 1;
  std::cout << m_day << "/" << m_month << "/" << m_year << "\n";
}

//renvoie la date d'aujourd'hui
const Date Date::getTodayDate(){
  time_t now = time(0);
  tm *dt = localtime(&now);
  m_today.m_day = dt->tm_mday;
  m_today.m_month = dt->tm_mon + 1;
  m_today.m_year = dt->tm_year + 1900;
  return m_today;
}

// retourne le nombre d'année entre la date passée en argument la date de cet objet
int Date::getAge(const Date &d) const{
  return abs(d.m_year - this->m_year);
}

