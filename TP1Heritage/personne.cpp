#include "personne.h"

Personne::Personne()
{
  m_nom = "";
  m_prenom = "";
  m_annif = Date();
}

Personne::~Personne()
{}

//constructeur par paramètres
Personne::Personne(const std::string &nom, const std::string &prenom, const Date &naiss):
  m_nom(nom), m_prenom(prenom), m_annif(naiss)
{}

//constructeur par copy
Personne::Personne(const Personne &p):m_nom(p.m_nom), m_prenom(p.m_prenom), m_annif(p.m_annif)
{}

//affectation
Personne Personne::operator=(const Personne &p){
  m_nom = p.m_nom;
  m_prenom = p.m_prenom;
  m_annif = p.m_annif;
  return *this;
}

//accesseur de nom
const std::string& Personne::getNom() const{
  return m_nom;
}

//accesseur de prenom
const std::string& Personne::getPrenom() const{
  return m_prenom;
}

//accesseur de age
int Personne::getAge() const{
  return m_annif.getAge(Date::getTodayDate());
  
}

//affiche
void Personne::print() const{
   std::cout << "tu t'appelles : " << m_nom << " " << m_prenom << " tu as "<< this->getAge() << " ans(s)"<< "\n";
}
