#include "employe.h"

// constructeur par défaut
Employe::Employe(): Personne(), m_salaire(0), m_nomEntreprise("")
{}

// Constructeur par paramètres
Employe::Employe(const std::string &nom, const std::string &prenom, const Date &naiss, const float salaire, const std::string &nomEnt) :
  Personne(nom, prenom, naiss), m_salaire(salaire), m_nomEntreprise(nomEnt)
{}

//constructeur par copy
Employe::Employe(const Employe &emp) :
  Personne(emp),
  m_salaire(emp.m_salaire),
  m_nomEntreprise(emp.m_nomEntreprise)
  {}

//construceur par affectation
Employe & Employe::operator=(const Employe &emp){
  this->Personne::operator=(emp);
  m_salaire = emp.m_salaire;
  m_nomEntreprise = emp.m_nomEntreprise;
  return *this;
}

Employe::~Employe(){}

std::string Employe::getNomEntre(){
  return m_nomEntreprise;
}

float Employe::getSalaire(){
  return m_salaire;
}

void Employe::setNomEntre(const std::string &nom)
{
  m_nomEntreprise = nom;
}

void Employe::setSalaire(const float sal){
  m_salaire = sal;
}


void Employe::print(){
  Personne::print();
  std::cout << "tu travailles à " << m_nomEntreprise << " tu as un salaire de " << m_salaire << "\n";
}
