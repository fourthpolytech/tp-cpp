#ifndef PERSONNE_H
#define PERSONNE_H

#include <iostream>
#include <string>

#include "date.h"

class Personne{
 protected :
  std::string m_nom ;
  std::string m_prenom;
  Date m_annif ;

 
  
 public:
  Personne();
  Personne(const std::string &nom, const std::string &prenom, const Date &naiss);
  virtual ~Personne();
  Personne(const Personne &p);
  Personne operator=(const Personne &p);
  const std::string& getNom() const;
  const std::string& getPrenom() const;
  int getAge() const;
  virtual void print() const;
  
};


#endif //PERSONNE_H
