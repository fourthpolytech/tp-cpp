#include "manager.h"

//constructeur par défaut
Manager::Manager(): Employe(),  m_capacite(0), m_equipe(NULL), m_taille(0),  m_primeAnnuelle(0)
{
  //m_equipe = NULL;
}

//constructeur par paramètres
Manager:: Manager(const std::string &nom, const std::string &prenom, const Date &naiss, const float salaire, const std::string &nomEnt, const int &capacite, const float &prime):
  
  Employe(nom, prenom, naiss, salaire, nomEnt),  m_capacite(capacite), m_equipe(new Employe[m_capacite]), m_taille(0), m_primeAnnuelle(prime)
{
  
  for(int i=0; i<m_capacite; i++){
    m_equipe[i] = Employe();
  }
}

// Constructeur par copy
Manager::Manager(const Manager &m) :  Employe(m), m_capacite(m.m_capacite),
				      m_equipe(new Employe[m_capacite]),m_taille(m.m_taille),
				      m_primeAnnuelle(m.m_primeAnnuelle)
{
  //copie du tableau d'employe
  for(int i=0; i<m_capacite; i++){
    m_equipe[i] = m.m_equipe[i];
  }
}

// Constructeur par affectation
Manager &Manager::operator= (const Manager &m)
{
  this->Employe::operator = (m);
  m_capacite = m.m_capacite;
  m_taille = m.m_taille;
  m_primeAnnuelle = m.m_primeAnnuelle;
  m_equipe = new Employe[m_capacite];
  //copie du tableau d'employe
  for(int i=0; i<m_capacite; i++){
    m_equipe[i] = m.m_equipe[i];
  }

  return *this;
}

// destructeur
Manager::~Manager(){
  // for(int i=0; i<m_capacite; i++){
  //   delete [] m_equipe;
  // }
  delete [] m_equipe;
}

// accesseur à tous les membres
const Employe* Manager::getEquipe()
{
  return m_equipe;
}

// mutateur de prime annuelle
void Manager::setPrimeAnnuelle(float prime)
{
  m_primeAnnuelle = prime;
}

// affiche 
void Manager::print(){
  std::cout << "print de manager\n";
  this->Employe::print();
  std::cout << " tu as une prime annuele de " << m_primeAnnuelle << " et tu diriges : \n";
  for(int i=0; i<m_taille; i++){
    m_equipe[i].print();
  }
}

//ajouter un membre
bool Manager::addToTeam(const Employe &e){
  if(m_taille >= m_capacite)
    return false;
  m_equipe[m_taille++] = e;
  std::cout << "taille actuelle = " << m_taille << "\n";
  return true;
}
