#ifndef ETUDIANT_H
#define ETUDIANT_H


#include <iostream>
#include <string>

#include "personne.h"

class Etudiant: public Personne{
 protected :
  std::string m_numCarteEtud;
  std::string m_nomUniv;

 public :
  Etudiant ();
  Etudiant(const std::string &nom, const std::string &prenom, const Date &naiss, 
	   const std::string &num, const std::string &univ);
  Etudiant(const Etudiant &etud);
  Etudiant & operator=(const Etudiant &etud);
  std::string getNum() const;
  std::string getUniv() const;
  void setNum(const std::string &num);
  void setUniv(const std::string &univ);
  virtual void print();

};

#endif //ETUDIANT_H
