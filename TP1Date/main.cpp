#include <iostream>

#include "date.h"

int main(){

  Date d (8, 2, 1994);
  d.afficheDate();
  Date tod = Date::getTodayDate();
  tod.afficheDate();
  int age = d.getAge(tod);
  std::cout << "tu as : " << age << "an(s)\n" ;
  
  return 0;
}
