#include "complex.hpp"
#include <iostream>

Complex createComplex(double re, double im){
    Complex c ;
    c.re = re;
    c.im = im;
    return c;
}

Complex add(Complex c1, Complex c2 ){
    Complex res ;
    res.im = c1.im + c2.im;
    res.re = c1.re + c2.re;
    return res;
}

Complex add(Complex c1, Complex c2, bool displable){
    Complex res = add(c1, c2);
    if(displable){
        printComplexe(res);
    }

    return res;
}

void add(Complex c1, Complex c2, Complex res){
    Complex *resP = &res;
    resP->im = c1.im + c2.im;
    resP->re = c1.re + c2.re;
}

void printComplexe(Complex c){
    if(c.im > 0)
        std::cout << c.re << " + " << c.im << "i" << std::endl;
    else if (c.im < 0)
            std::cout << c.re << " " << c.im << "i" << std::endl;
         else
            std::cout << c.re << std::endl;
}
