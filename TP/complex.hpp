#ifndef COMPLEX_INCLUDED
#define COMPLEX_INCLUDED

#include <iostream>
#include <cstdio>

typedef struct cpx {
    double im, re;
} Complex;

Complex createComplex(double re, double im);
Complex add(Complex c1, Complex c2 );
void add(Complex c1, Complex c2, Complex res);
Complex add(Complex c1, Complex c2, bool displable);
void printComplexe(Complex c);


#endif // COMPLEX_INCLUDED
