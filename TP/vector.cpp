#include<iostream>
#include<cstdlib>

#include "vector.hpp"

 Vector:: Vector()
{
    m_v = new int[0];
}

Vector::Vector(int size): m_size(size){
    m_v = new int[m_size];
}

 Vector::~ Vector()
{
    delete m_v;
}

int Vector::getElt(int index)const {
    if(!isInLimits(index, 0, m_size-1)){
        std::cout << index << " is out the limit" << std::endl;
        std::exit(-1);
    }
    return m_v[index];
}

void Vector::setElt(int index, int value){
    if(!isInLimits(index, 0, m_size-1)){
        std::cout << index << " is out the limit" << std::endl;
        std::exit(-1);
    }

    m_v[index] = value;
}

void Vector::print() const {
    for(int i=0; i<m_size; i++) {
        std::cout << m_v[i] << " " ;
    }
    std::cout << std::endl;
}

inline bool Vector::isInLimits(int idx, int lim1, int lim2) const{
    return lim1 <= idx && idx <= lim2;
} 