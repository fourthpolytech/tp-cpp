#include <iostream>

#include "vector.hpp"


int main(int argc, char *argv[]) {
    Vector v = Vector(3);
    v.setElt(0, 2);
    v.setElt(1, 5);
    v.setElt(3, 1);

    v.print();
}