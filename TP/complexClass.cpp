#include<iostream>

#include "complexClass.hpp"

ComplexClass::ComplexClass() : m_rel(0), m_im(0)
{}

ComplexClass::ComplexClass(float rel, float im) : m_rel(rel), m_im(im)
{}

ComplexClass::~ComplexClass()
{}

ComplexClass ComplexClass::add(ComplexClass const &comp){
    m_rel += comp.m_rel; 
    m_im += comp.m_im;

    return ComplexClass(m_rel, m_im);
}

void ComplexClass::print() const{
    if(m_im > 0)
        std::cout << m_rel << " + " << m_im << "i" << std::endl;
    else if (m_im < 0)
            std::cout << m_rel << " " << m_im << "i" << std::endl;
         else
            std::cout << m_rel << std::endl;
}