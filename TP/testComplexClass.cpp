#include <iostream>

#include "complexClass.hpp"

int main(int argc, char *argv[]){
    ComplexClass *c1 = new ComplexClass();
    ComplexClass *c2 = new ComplexClass(3.0f, 4.0f);
    ComplexClass *c3 = new ComplexClass(5.0f, 3.0f);

    c1->print();
    c2->print();
    c3->print();

    c1->add(*c2);
    std::cout << "c1 add c2 " << std::endl;
    c1->print();

    ComplexClass c4 = ComplexClass();
    c4 = c4.add(*c2).add(*c3);
    std::cout << "c4.add(c2).add(c3) "<< std::endl;
    c4.print();
}