class ComplexClass
{
private:
    /* data */
    float m_rel, m_im;
public:
    ComplexClass();
    ComplexClass(float rel, float im);
    ~ComplexClass();

    ComplexClass add(ComplexClass const &comp);
    void print() const;
};