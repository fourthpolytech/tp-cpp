#include <iostream>

class Ticket
{
private:
    /* data */
    static int nextCreatedTicket;
    static int nextCalledTicket;
    int id;
public:
    
    Ticket(/* args */);
    ~Ticket();
    bool isMyTurn() const;
    static int getNextCreated();
    static int getNextCalled();
};


