#include<iostream>
#include "ticket.hpp"

int Ticket::nextCalledTicket = 0;
int Ticket::nextCreatedTicket = 0;

Ticket::Ticket(/* args */)
{
    id = ++nextCreatedTicket;
}

Ticket::~Ticket()
{
    nextCalledTicket++;
}

int Ticket::getNextCreated(){
    return nextCreatedTicket;
}

int Ticket::getNextCalled(){
    return nextCalledTicket;
}

bool Ticket::isMyTurn() const{
    return nextCalledTicket == id;
}