class  Vector
{
private:
    /* data */
    int m_size;
    int *m_v;
public:
     Vector();
     Vector(int size);
    ~Vector();

    int getElt(int index)const;
    void setElt(int index, int value);
    void print() const;
    bool isInLimits(int idx, int lim1, int lim2) const;
};